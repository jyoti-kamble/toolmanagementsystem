import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Login from "../Login/Login";
import Home from "../Pages/Home";

const Main = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/home" component={Home} />
        </Switch>
      </Router>
    </div>
  );
};
export default Main;
