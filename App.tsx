import React from 'react';
import Login from './Component/Login/Login';
import styles from './App.module.scss';
import Main from './Component/Main/Main';

function App() {
  return (
    <div className={styles.App}>
      <Main/>
    </div>
  );
}

export default App;
